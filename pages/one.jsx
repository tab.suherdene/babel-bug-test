import React, { useState, useEffect, Component } from "react";

let Line = null
if (process.browser) {
  const antDesignChart = require('@ant-design/charts')
  Line = antDesignChart.Line
  // antsDesignChart.setAntChart(antsDesignChart.antChart)
}

export default class One extends Component {
  Line = Line
  constructor(props) {
    super(props)
    if (process.browser) {
      this.state = {}
      const data = [
        { year: '1991', value: 3 },
        { year: '1992', value: 4 },
        { year: '1993', value: 3.5 },
        { year: '1994', value: 5 },
        { year: '1995', value: 4.9 },
        { year: '1996', value: 6 },
        { year: '1997', value: 7 },
        { year: '1998', value: 9 },
        { year: '1999', value: 13 },
      ];

      this.state.config = {
        data,
        width: 800,
        height: 400,
        autoFit: false,
        xField: 'year',
        yField: 'value',
        point: {
          size: 5,
          shape: 'diamond',
        },
        label: {
          style: {
            fill: '#aaa',
          },
        },
      };
    }
  }

  render() {
    return (
      <div suppressHydrationWarning={true}>
        {process.browser && <Line {...this.state.config} />}
      </div>
    )
  }

}

